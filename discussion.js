//

db.inventory.insertMany([
		{
			"name": "Javascript for Beginners",
			"author": "James Doe",
			"price": 5000,
			"stocks": 50, 
			"publisher": "JS Publishing House"
		},

		{
			"name": "HTML and CSS",
			"author": "John Thomas",
			"price": 2500,
			"stocks": 38, 
			"publisher": "NY Publishers"
		},

		{
			"name": "Web Development Fundamentals",
			"author": "Noah Jimenez",
			"price": 3000,
			"stocks": 10, 
			"publisher": "Big Idea Publishing"
		},

		{
			"name": "Java Programming",
			"author": "David Michael",
			"price": 10000,
			"stocks": 100, 
			"publisher": "JS Publishing House"
		}

	])


//Comparison Query Operators
//$gt/$gte operator
/*
	Syntax: 
		db.collection.find({"field": {$gt: value}})
		db.collection.find({"field": {$gte: value}})

*/


db.inventory.find({
	"stocks": {
		$gt: 50
	}
})

db.inventory.find({
	"stocks": {
		$gte: 50
	}
})



//$lt/$lte operator

/*
	Syntax:
		db.collectionName.find({ "field": {$lt: value}})
		db.collectionName.find({ "field": {$lte: value}})

*/


db.inventory.find({
	"stocks": {
		$lt: 50
	}
})


//$ne operator - not equal
/*
	Syntax:
		db.collectionName.({ "field": {$ne: value}})
*/


db.inventory.find({
	"stocks": {
		$ne: 50
	}
})


//$eq operator - equal
/*
	Syntax:
		db.collectionName.({ "field": {$eq: value}})
*/


db.inventory.find({
	"stocks": {
		$eq: 50
	}
})


//$in operator 
/*
	Syntax:
		db.collectionName.({ "field": $in: [value1, value2] }})
*/


db.inventory.find({
	"price": {
		$in: [10000, 5000]
	}
})


//$nin operator 
/*
	Syntax:
		db.collectionName.({ "field": $nin: [value1, value2] }})
*/



db.inventory.find({
	"price": {
		$nin: [10000, 5000]
	}
})


//range
db.inventory.find({
	"price": {$gt: 2000} && {$lt: 4000} 
})


db.inventory.find({
	"price": {
		$lte: 4000
	}
})

db.inventory.find({
	"stocks": {
		$in: [50, 100]
	}
})


//Logical Query Operators
/*
$or operator

	Syntax:
		db.collectionName.find({
			$or: [
				{"fieldA": "valueA"},
				{"fieldB": "valueB"},
			]
		})


*/


db.inventory.find({
	$or: [
		{"name": "HTML and CSS"},
		{"publisher": "JS Publishing House" }

		]
})


db.inventory.find({
	$or: [
		{"author": "James Doe"},
		{"price": {
			$lte: 5000
		}}
		
		]
})

/*
$and operator

	Syntax:
		db.collectionName.find({
			$and: [
				{"fieldA": "valueA"},
				{"fieldB": "valueB"},
			]
		})


*/


db.inventory.find({
		$and: [
				{"stocks": {
				$ne:50
			}
		},
				{"price": {
				$ne:5000
			}
		},

	]
})



db.inventory.find({
				"stocks": {
				$ne:50
			},
		
				"price": {
				$ne:5000
			},

})


//Field Projection

//Inclusion
/*
	Syntax:
		db.collectionName.find({criteria}, {field: 1})
*/

db.inventory.find({
	"publisher": "JS Publishing House"
	},

	{
		"name": 1,
		"author": 1,
		"price":1

}
)

//Exclusion - hindi pwede pagsabayin si field inclusion at field exclusion
/*
	Syntax:
		db.collectionName.find({criteria}, {field: 0})

*/


db.inventory.find(
		{
			"author": "Noah Jimenez"
		},

		{
			"price": 0,
			"stocks": 0
		}

	)

db.inventory.find(
		{
			"price": {
				$lte:5000
			}
		},

		{
			"_id": 0,
			"name": 1,
			"author": 1
		}
	)


//Evaluation Query Operator
//$regex oeprator

/*
	Syntax: 
		db.collectionName.find({ field: {
			$regex: 'pattern', $options: 'optionValue'
		}})
	
*/

db.inventory.find({
	"author": {
		$regex: 'M'
	}
})


//Case insensitive
db.inventory.find({
	"author": {
		$regex: 'M',
		$options: '$i'
	}
})


//updateOne({criteria/query}, {set})

//updateMany({criteria/query}, {set})

//deleteOne({criteria/query})
//deleteMany({criteria/query})